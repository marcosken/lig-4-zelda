# Lig-4 Zelda

Link do site: https://marcosken.gitlab.io/lig-4-zelda

## Sobre o Projeto

Esse projeto foi realizado como parte integrante das atividades de ensino na Kenzie Academy Brasil.
O objetivo era trabalhar em equipe e desenvolver o jogo Lig-4 utilizando os conceitos de HTML, CSS e JavaScript aprendidos no curso.

Equipe: Marcos Kuribayashi, Maykel Nekel e Vinícius Troyack.

## Regras do jogo

No Lig-4, dois jogadores assumem peças diferentes (geralemente diferenciadas por cores ou formatos). Os jogadores se alternam inserindo suas peças em uma das 7 colunas de uma tabela 7x6. O primeiro jogador que conseguir quatro de suas peças em uma linha (seja horizontal, vertical ou diagonal) vence. O jogo pode terminar em empate quando todas as células estiverem preenchidas e nenhum jogador conseguir quatro peças em linha.

## Conhecimentos aplicados

∙ HTML\
∙ CSS\
∙ JavaScript\
∙ Manipulação do DOM
